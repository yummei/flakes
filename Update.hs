{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}

module Update (main) where

import Control.Monad (unless)
import qualified Data.Aeson as A
import Data.Default (def)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Text (Text)
import qualified Data.Text as T
import Development.Shake
import NeatInterpolation (trimming)
import NvFetcher
import NvFetcher.Config (Config (actionAfterBuild))

main :: IO ()
main = runNvFetcher' def {actionAfterBuild = generateReadme >> processAutoCommit >> processShellAutoCommit} packageSet

packageSet :: PackageSet ()
packageSet = do
  -----------------------------------------------------------------------------
  -- psiphon
  define $
    -- A synonym of 'fetchGitHub' and 'sourceGitHub'
    package "psiphon" `fromGitHub` ("Psiphon-Labs", "psiphon-tunnel-core")
  -----------------------------------------------------------------------------
  -- phantomsocks
  define $
    package "phantomsocks"
      -- use sourceGit when no tag
      `sourceGit` "https://github.com/macronut/phantomsocks"
      `fetchGitHub` ("macronut", "phantomsocks")
  -----------------------------------------------------------------------------
  -- bili_you
  define $
    package "biliyou"
      `sourceGitHub` ("lucinhu", "bili_you")
      `fetchGitHub'` ("lucinhu", "bili_you", fetchSubmodules .~ True)

-- generated commit message in GITHUB_ENV
processAutoCommit :: Action ()
processAutoCommit =
  getEnv "GITHUB_ENV" >>= \case
    Just env -> do
      changes <- getVersionChanges
      liftIO $
        unless (null changes) $
          appendFile env $
            "COMMIT_MSG<<EOF\n"
              <> case show <$> changes of
                [x] -> x <> "\n"
                xs -> "Auto update:\n" <> unlines xs
              <> "EOF\n"
    _ -> pure ()

-- generated commit message in SHELL_ENV
processShellAutoCommit :: Action ()
processShellAutoCommit =
  getEnv "SHELL_ENV" >>= \case
    Just env -> do
      changes <- getVersionChanges
      liftIO $
        unless (null changes) $
          appendFile env $
            "export " <> "COMMIT_MSG=\""
              <> case show <$> changes of
                [x] -> x <> "\n"
                xs -> "Auto update:\n" <> unlines xs
              <> "\"\n"
    _ -> pure ()

-- generated README.md
generateReadme :: Action ()
generateReadme = do
  -- we need use generated files in flakes
  command [] "git" ["add", "."] :: Action ()
  (A.decode @(Map Text Text) -> Just (Map.elems -> out)) <-
    fromStdout
      <$> command
        []
        "nix"
        [ "eval",
          "./#packages.x86_64-linux",
          "--apply",
          T.unpack [trimming|with builtins; mapAttrs (key: value: "[$${key}](${value.meta.homepage or ""}) - ${value.version}")|],
          "--json"
        ]
  template <- T.pack <$> readFile' "README_template.md"
  writeFileChanged "README.md" $ T.unpack $ template <> "\n" <> (T.unlines $ map ("* " <>) out) 
  putInfo "Generate README.md"
