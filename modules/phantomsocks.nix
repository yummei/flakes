{ lib, pkgs, config, ... }:
with lib;                      
let
  cfg = config.services.phantomsocks;
  user = "phantomsocks";
  group = "phantomsocks";
in {
  options.services.phantomsocks = {
    enable = mkEnableOption "enable phantomsocks service, the configuration directory is `/etc/phantomsocks`";
    configFolder = mkOption {
      type = types.str;
      default = "/etc/phantomsocks";
      description = lib.mdDoc ''
          Path to storage of phantomsocks config.
      '';
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [ pkgs.phantomsocks ];

    users.users.${user} = {
      description  = "Phantomsocks Daemon User";
      isSystemUser = true;
      inherit group;
    };

    users.groups.${group} = { };

    systemd.services.phantomsocks = {
      description = "Phantomsocks Service";
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        User = user;
        # DynamicUser = true;
        Group = group;
        StateDirectory = [ "phantomsocks" ];
        # %E/phantomsocks/
        WorkingDirectory = "${cfg.configFolder}";
        ExecStart = "${pkgs.phantomsocks}/bin/phantomsocks";
        Restart = "on-failure";
        # Restart = "on-abnormal";
      };
    };
  };
}
