{ lib, pkgs, config, ... }:
with lib;                      
let
  cfg = config.services.psiphon;
  settingsFormat = pkgs.formats.json {};
  clientSettingsFile = settingsFormat.generate "client.config" cfg.client.settings;
  runDir = "/run/psiphon";
  stateDir = "/var/lib/psiphon";
in {
  options.services.psiphon = {
    enable = mkEnableOption "enable psiphon service";
    server = {
      enable = mkEnableOption "enable psiphon server service";
    };
    client = {
      enable = mkEnableOption "enable psiphon client service, the configuration directory is `/etc/psiphon`";
      settings = mkOption {
        default = {};
        description = lib.mdDoc ''
            psiphon config. The attributes are serialized to JSON used as client.config.
            See [psiphon config](https://github.com/Psiphon-Labs/psiphon-tunnel-core/blob/master/psiphon/config.go)
        '';
        type = types.submodule {
          freeformType = settingsFormat.type;
          options.LocalHttpProxyPort = mkOption {
            type = with types; nullOr port;
            default = 8081;
            description = lib.mdDoc ''
              LocalHttpProxyPort specifies a port number for the local HTTP
              proxy running at 127.0.0.1. For the default value, 0, the
              system selects a free port (a notice reporting the selected
              port is emitted).
            '';
          };
          options.LocalSocksProxyPort = mkOption {
            type = with types; nullOr port;
            default = 1081;
            description = lib.mdDoc ''
              LocalSocksProxyPort specifies a port number for the local SOCKS
              proxy running at 127.0.0.1. For the default value, 0, the
              system selects a free port (a notice reporting the selected
              port is emitted).
            '';
          };
          options.PropagationChannelId = mkOption {
            type = types.str;
            default = "FFFFFFFFFFFFFFFF";
            description = lib.mdDoc ''
              PropagationChannelId is a string identifier which indicates how
              the Psiphon client was distributed. This parameter is required.
              This value is supplied by and depends on the Psiphon Network,
              and is typically embedded in the client binary.
            '';
          };
          options.RemoteServerListDownloadFilename = mkOption {
            type = with types; nullOr str;
            default = "remote_server_list";
            description = lib.mdDoc ''
              Deprecated:
              Use MigrateRemoteServerListDownloadFilename. When
              MigrateRemoteServerListDownloadFilename is set, this parameter
              is ignored.

              RemoteServerListDownloadFilename specifies a target filename
              for storing the remote server list download. Data is stored
              in co-located files (RemoteServerListDownloadFilename.part*)
              to allow for resumable downloading.
            '';
          };
          options.RemoteServerListSignaturePublicKey = mkOption {
            type = with types; nullOr str;
            default = "MIICIDANBgkqhkiG9w0BAQEFAAOCAg0AMIICCAKCAgEAt7Ls+/39r+T6zNW7GiVpJfzq/xvL9SBH5rIFnk0RXYEYavax3WS6HOD35eTAqn8AniOwiH+DOkvgSKF2caqk/y1dfq47Pdymtwzp9ikpB1C5OfAysXzBiwVJlCdajBKvBZDerV1cMvRzCKvKwRmvDmHgphQQ7WfXIGbRbmmk6opMBh3roE42KcotLFtqp0RRwLtcBRNtCdsrVsjiI1Lqz/lH+T61sGjSjQ3CHMuZYSQJZo/KrvzgQXpkaCTdbObxHqb6/+i1qaVOfEsvjoiyzTxJADvSytVtcTjijhPEV6XskJVHE1Zgl+7rATr/pDQkw6DPCNBS1+Y6fy7GstZALQXwEDN/qhQI9kWkHijT8ns+i1vGg00Mk/6J75arLhqcodWsdeG/M/moWgqQAnlZAGVtJI1OgeF5fsPpXu4kctOfuZlGjVZXQNW34aOzm8r8S0eVZitPlbhcPiR4gT/aSMz/wd8lZlzZYsje/Jr8u/YtlwjjreZrGRmG8KMOzukV3lLmMppXFMvl4bxv6YFEmIuTsOhbLTwFgh7KYNjodLj/LsqRVfwz31PgWQFTEPICV7GCvgVlPRxnofqKSjgTWI4mxDhBpVcATvaoBl1L/6WLbFvBsoAUBItWwctO2xalKxF5szhGm8lccoc5MZr8kfE0uxMgsxz4er68iCID+rsCAQM=";
            description = lib.mdDoc ''
              RemoteServerListSignaturePublicKey specifies a public key that's
              used to authenticate the remote server list payload. This value
              is supplied by and depends on the Psiphon Network, and is
              typically embedded in the client binary.
            '';
          };
          options.RemoteServerListUrl = mkOption {
            type = with types; nullOr str;
            default = "https://s3.amazonaws.com//psiphon/web/mjr4-p23r-puwl/server_list_compressed";
            description = lib.mdDoc ''
              Deprecated:
              Use RemoteServerListURLs. When RemoteServerListURLs is not
              nil, this parameter is ignored.

              RemoteServerListUrl is a URL which specifies a location to
              fetch out-of-band server entries. This facility is used when a
              tunnel cannot be established to known servers. This value is
              supplied by and depends on the Psiphon Network, and is typically
              embedded in the client binary.
            '';
          };
          options.SponsorId = mkOption {
            type = with types; nullOr str;
            default = "FFFFFFFFFFFFFFFF";
            description = lib.mdDoc ''
              SponsorId is a string identifier which indicates who is sponsoring
              this Psiphon client. One purpose of this value is to
              determine the home pages for display. This parameter is required.
              This value is supplied by and depends on the Psiphon Network,
              and is typically embedded in the client binary.
            '';
          };
          options.UseIndistinguishableTLS = mkOption {
            type = with types; nullOr bool;
            default = true;
            description = lib.mdDoc ''
              Removed.
            '';
          };
          options.ListenInterface = mkOption {
            type = with types; nullOr str;
            default = null;
            example = "0.0.0.0";
            description = lib.mdDoc ''
              ListenInterface specifies which interface to listen on.  If no
              interface is provided then listen on 127.0.0.1. If 'any' is
              provided then use 0.0.0.0. If there are multiple IP addresses
              on an interface use the first IPv4 address.
            '';
          };
        };
      };
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [ pkgs.psiphon ];
    systemd.services.psiphon-server = mkIf cfg.server.enable {
      description = "Psiphon Proxy Server Service";

      wantedBy = [ "multi-user.target" ];
      after    = [ "network.target" ];

      serviceConfig = {
        DynamicUser = true;
        # RuntimeDirectoryMode = "0710";
        RuntimeDirectory = [
          # g+x allows access to the control socket
          "psiphon"
          "psiphon/root"
        ];
        # StateDirectoryMode = "0700";
        StateDirectory = [
            "psiphon"
        ];
        ExecStart = "${pkgs.psiphon}/bin/psiphon-server run";
        Restart = "always";
        WorkingDirectory = stateDir;
      };
    };
    systemd.services.psiphon-client = mkIf cfg.client.enable {
      description = "Psiphon Proxy Client Service";

      wantedBy = [ "multi-user.target" ];
      after    = [ "network.target" ];

      restartTriggers = [ clientSettingsFile ];

      serviceConfig = {
        DynamicUser = true;
        # RuntimeDirectoryMode = "0710";
        RuntimeDirectory = [
          # g+x allows access to the control socket
          "psiphon"
          "psiphon/root"
        ];
        # StateDirectoryMode = "0700";
        StateDirectory = [ "psiphon" ];
        ExecStart = "${pkgs.psiphon}/bin/psiphon-console-client -config ${clientSettingsFile} -formatNotices";
        Restart = "always";
        WorkingDirectory = stateDir;
      };
    };
  };
}
