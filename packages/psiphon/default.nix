{ lib
, buildGo119Module
, fetchFromGitHub
, mySource
}:


# cannot use "The version of quic-go you're using
# can't be built on Go 1.20 yet. For more details, please see
# https://github.com/lucas-clemente/quic-go/wiki/quic-go-and-Go-versions."

buildGo119Module rec {
  inherit (mySource) pname version src;

  vendorSha256 = null;
  
  ldflags = [
    "-s" "-w"
    "-X main.Version=v${version}"
    "-X github.com/Psiphon-Labs/psiphon-tunnel-core/psiphon/common.buildRepo=nixpkgs"
  ];
  # We only need client and server.
  subPackages = [ "ConsoleClient" "Server" ];

  postInstall = ''
    mv $out/bin/ConsoleClient $out/bin/psiphon-console-client
    mv $out/bin/Server $out/bin/psiphon-server
  '';

  meta = with lib; {
    description = "Psiphon is an Internet censorship circumvention system.";
    longDescription = ''
      Psiphon tunnel core project includes a tunneling client and server,
      which together implement key aspects of evading blocking and relaying
      client traffic through Psiphon and beyond censorship.
    '';
    homepage = "https://github.com/Psiphon-Labs/psiphon-tunnel-core";
    license = licenses.gpl3Only;
    maintainers = with maintainers; [ ];
  };
}
