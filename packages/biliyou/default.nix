{ lib
, flutter
, fetchFromGitHub
, pkg-config
, makeDesktopItem
, imagemagick
, mpv
, gtk3
, mySource
}:

flutter.buildFlutterApplication rec {
  inherit (mySource) pname version src;
  
  # fetchSubmodules = true;

  buildInputs = [
    mpv
    gtk3
  ];
  nativeBuildInputs = [
    pkg-config
    imagemagick
  ];

  # depsListFile = ./deps.json;
  autoDepsList = true;
  # 1.1.5
  vendorHash = "sha256-keQf4955n7ugfgZ4uyEDdB6/Jrqi/U0xkUWdNWCTCTk=";

  desktopItem = makeDesktopItem {
    name = "bili_you";
    exec = "@out@/bin/bili_you";
    icon = "bili_you";
    desktopName = "bili_you";
    genericName = "Watch video in bilibili";
    categories = [ "Network" "Video" "AudioVideo" ];
  };
  postInstall = ''
    FAV=$out/app/data/flutter_assets/assets/icon/bili.png
    ICO=$out/share/icons
    install -D $FAV $ICO/bili_you.png
    mkdir $out/share/applications
    cp $desktopItem/share/applications/*.desktop $out/share/applications
    for size in 24 32 42 64 128 256 512; do
      D=$ICO/hicolor/''${s}x''${s}/apps
      mkdir -p $D
      convert $FAV -resize ''${size}x''${size} $D/bili_you.png
    done
    substituteInPlace $out/share/applications/*.desktop \
      --subst-var out
  '';


  meta = with lib; {
    description = "一个用 flutter 制作的第三方B站客户端.";
    homepage = "https://github.com/lucinhu/bili_you";
    license = licenses.asl20;
    maintainers = with maintainers; [ yummei ];
  };
}
