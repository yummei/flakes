{ lib
, buildGoModule
, fetchFromGitHub
, stdenv
, libpcap
# Cann't be build with both pcap and rawsocket tags
, withPcap ? (!stdenv.isLinux && !withRawsocket)
, withRawsocket ? (stdenv.isLinux && !withPcap)
, mySource
}:

buildGoModule rec {
  inherit (mySource) pname version src;

  vendorHash = "sha256-0MJlz7HAhRThn8O42yhvU3p5HgTG8AkPM0ksSjWYAC4=";

  ldflags = [
    "-s" "-w"
  ];
  buildInputs = lib.optional withPcap libpcap;
  tags = lib.optional withPcap "pcap"
    ++ lib.optional withRawsocket "rawsocket";

  meta = with lib;{
    homepage = "https://github.com/macronut/phantomsocks";
    description = "A cross-platform proxy client/server for Linux/Windows/macOS";
    longDescription = ''
      A cross-platform proxy tool that could be used to modify TCP packets
      to implement TCB desync to bypass detection and censoring.
    '';
    license = licenses.lgpl3Only;
    maintainers = with maintainers; [ oluceps ];
  };
}
